FROM gitpod/workspace-full-vnc

USER gitpod

RUN sudo apt-get update && pyenv install 3.10.9 && pyenv global 3.10.9
RUN python3 -m pip install --upgrade pip && python3 -m pip install pipenv
import json
import logging
from collections import defaultdict
from typing import List

import networkx as nx

from warehouse_system.cstes import BoundaryTypes, Directions
from warehouse_system.room import Room

logger = logging.getLogger(__name__)


def _sanitize_coord(arg):
    if not isinstance(arg, int):
        raise ValueError("A coordinate must be an int")
    return arg


def _sanitize_direction(arg):
    if not isinstance(arg, str) and not isinstance(arg, Directions):
        raise ValueError(
            f"Directions must be strings ('{Directions.NORTH.value}', '{Directions.EAST.value}', '{Directions.WEST.value}' or '{Directions.SOUTH.value}')"
        )
    if isinstance(arg, str):
        arg = arg.lower()
        arg = Directions(arg)

    return arg


def _sanitize_boundary_type(arg):
    if not isinstance(arg, str) and not isinstance(arg, BoundaryTypes):
        raise ValueError(
            f"A boundary type must be a string (one of '{BoundaryTypes.PATH}', '{BoundaryTypes.WALL}' or '{BoundaryTypes.UNKNOWN}')"
        )
    if isinstance(arg, str):
        arg = arg.lower()
        arg = BoundaryTypes(arg)
    if arg not in (BoundaryTypes.PATH, BoundaryTypes.WALL, BoundaryTypes.UNKNOWN):
        raise ValueError(
            f"A boundary type must be either '{BoundaryTypes.PATH}', '{BoundaryTypes.WALL}' or '{BoundaryTypes.UNKNOWN}'"
        )
    return arg


def _sanitize_optional_boundary_type(arg):
    if arg is None:
        return None
    return _sanitize_boundary_type(arg)


_sanitize_single_arg = {
    "coord": _sanitize_coord,
    "direction": _sanitize_direction,
    "boundary_type": _sanitize_boundary_type,
    "optional_boundary_type": _sanitize_optional_boundary_type,
}


def sanitize(*args_descriptions):
    def decorator(func):
        def wrapped_func(self, *args):
            new_args = []
            for index, description in enumerate(args_descriptions):
                if description == "directions":
                    new_args.append(_sanitize_single_arg[description](args[index:]))
                else:
                    new_args.append(_sanitize_single_arg[description](args[index]))
            return func(self, *new_args)

        return wrapped_func

    return decorator


def other_room(x, y, direction):
    match direction:
        case Directions.NORTH:
            return x, y - 1, Directions.SOUTH
        case Directions.SOUTH:
            return x, y + 1, Directions.NORTH
        case Directions.EAST:
            return x + 1, y, Directions.WEST
        case Directions.WEST:
            return x - 1, y, Directions.EAST


class Warehouse:
    def __init__(self):
        self._rooms = defaultdict(Room)
        self._graph = nx.Graph()

    @property
    def graph(self):
        return self._graph

    def get_room(self, x, y) -> Room:
        self._graph.add_node((x, y))
        return self._rooms[x, y]

    # could be deleted
    def get_capacity(self, x, y):
        return self.get_room(x, y).get_capacity()

    # could be deleted
    def get_n_boundaries(self, x, y, boundary_type):
        return self.get_room(x, y).get_n_boundaries(boundary_type)

    # could be deleted
    def set_room_type(self, x, y, room_type):
        logger.debug(f"set_room_type({x}, {y}, {room_type})")
        return self.get_room(x, y).set_room_type(room_type)

    # could be deleted
    def get_room_type(self, x, y):
        return self.get_room(x, y).get_room_type()

    # could be deleted
    def get_storage(self, x, y):
        return self.get_room(x, y).get_storage()

    # could be deleted
    def set_storage(self, x, y, items):
        logger.debug(f"set_storage({x}, {y})")
        return self.get_room(x, y).set_storage(items)

    # could be deleted
    def get_stored_item_type(self, x, y):
        return self.get_room(x, y).get_stored_item_type()

    # could be deleted
    def add_storage_item(self, x, y, item: str):
        return self.get_room(x, y).add_storage_item(item)

    # could be deleted
    def add_storage_items(self, x, y, items: List[str]):
        return self.get_room(x, y).add_storage_items(items)

    def pop_storage_item(self, x, y):
        return self.get_room(x, y).pop_storage_item()

    # could be deleted
    def set_max_capacity(self, x, y, max_capacity):
        logger.debug(f"set_max_capacity({x}, {y}, {max_capacity})")
        return self.get_room(x, y).set_max_capacity(max_capacity)

    # could be deleted
    def get_max_capacity(self, x, y):
        return self.get_room(x, y).get_max_capacity()

    # could be deleted
    def get_boundaries(self, x, y):
        return self.get_room(x, y).get_boundaries()

    # could be deleted
    def get_storage_count(self, x, y) -> int:
        return self.get_room(x, y).get_storage_count()

    def get_n_walls(self, x, y):
        return self.get_n_boundaries(x, y, BoundaryTypes.WALL)

    def get_n_paths(self, x, y):
        return self.get_n_boundaries(x, y, BoundaryTypes.PATH)

    def get_n_unknowns(self, x, y):
        return self.get_n_boundaries(x, y, BoundaryTypes.UNKNOWN)

    @sanitize("coord", "coord", "direction", "boundary_type")
    def set_boundary(self, x, y, direction, boundary_type):
        logger.debug(f"set_boundary({x}, {y}, {direction}, {boundary_type})")
        self.get_room(x, y).set_boundary(direction, boundary_type)
        other_x, other_y, other_direction = other_room(x, y, direction)
        self.get_room(other_x, other_y).set_boundary(other_direction, boundary_type)
        if boundary_type is BoundaryTypes.PATH:
            other_x, other_y, other_direction = other_room(x, y, direction)
            self._graph.add_edge((x, y), (other_x, other_y), direction=direction)

    @sanitize("coord", "coord", "direction")
    def delete_boundary(self, x, y, direction):
        self.get_room(x, y).delete_boundary(direction)
        other_x, other_y, other_direction = other_room(x, y, direction)
        self.get_room(other_x, other_y).delete_boundary(other_direction)
        other_x, other_y, other_direction = other_room(x, y, direction)
        if self._graph.has_edge((x, y), (other_x, other_y)):
            self._graph.remove_edge((x, y), (other_x, other_y))

    @sanitize("coord", "coord", "boundary_type", "boundary_type", "boundary_type", "boundary_type")
    def set_boundaries(self, x, y, north, east, south, west):
        logger.debug(f"set_boundaries({x}, {y}, {east}, {south}, {west})")
        self.set_boundary(x, y, Directions.NORTH, north)
        self.set_boundary(x, y, Directions.EAST, east)
        self.set_boundary(x, y, Directions.SOUTH, south)
        self.set_boundary(x, y, Directions.WEST, west)

    def get_total_known_capacity(self):
        total = 0
        for (x, y), room in self._rooms.items():
            if room.get_n_boundaries(BoundaryTypes.UNKNOWN) == 0:
                total += room.get_capacity()

    @property
    def total_known_capacity(self):
        return self.get_total_known_capacity()

    def serialize(self):
        ret = {"rooms": []}
        for (x, y), room in self._rooms.items():
            if not room.get_boundaries(BoundaryTypes.UNKNOWN) == 4:
                ret["rooms"].append({"coord": {"x": x, "y": y}, "room": room.serialize()})

    @classmethod
    def deserialize(cls, content):
        warehouse = cls()
        for room_content in content["rooms"]:
            x = room_content["coord"]["x"]
            y = room_content["coord"]["y"]
            boundaries = Room.deserialize(room_content["room"]).get_boundaries()
            warehouse.set_boundaries(x, y, *boundaries)

    def dump(self, fp):
        serialized_data = self.serialize()
        json.dump(serialized_data, fp)

    @classmethod
    def load(cls, fp):
        content = json.load(fp)
        return cls.deserialize(content)

import logging

logger = logging.getLogger(__name__)


class SequenceManager:
    def __init__(self, robot, robot_api, warehouse, planner, action_manager):
        self.robot = robot
        self.robot_api = robot_api
        self.warehouse = warehouse
        self.planner = planner

    def pick_and_place(self):
        if not self.go_to_ramp():
            logger.warning("Could not pick and place: could not go to ramp")
            return False

        if not self.action_manager.pick():
            logger.warning("Could not pick and place: could not pick")
            return False

        robot_item = self.robot.get_storage()
        other_x, other_y = self.planner.get_item_place_location(robot_item)

        if not self.go_to(other_x, other_y):
            logger.warning(f"Could not pick and place: could not go to {(other_x, other_y)}")
            return False

        if not self.action_manager.place():
            logger.warning("Could not pick and place: could not pick")
            return False

    def go_to(self, other_x, other_y):
        x, y = self.robot.get_coord()
        path = self.planner.find_path(x, y, other_x, other_y)
        if path is None:
            logger.warning(f"Could not find a path from {(x, y)} to {(other_x, other_y)}")
            return
        for direction in path:
            self.action_manager.move(direction)

    def go_to_ramp(self):
        if not self.warehouse.found_ramp():
            logger.warning("Could not go to ramp: I don't know where the ramp is")
            return False
        other_x, other_y = self.warehouse.get_ramp_coord()
        return self.go_to(other_x, other_y)

    def explore(self):
        pass

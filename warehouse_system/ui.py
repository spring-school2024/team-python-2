import pygame

from warehouse_system import configure_logging
from warehouse_system.action_manager import ActionManager
from warehouse_system.james_api import JamesAPI
from warehouse_system.presenter import Presenter
from warehouse_system.rest_api import RestAPI
from warehouse_system.robot import Robot
from warehouse_system.robot_api import RobotAPI
from warehouse_system.warehouse import Warehouse
from warehouse_system.warehouse_view import WarehouseView


def main():
    configure_logging()
    pygame.init()
    base_url = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/api/"

    rest_api = RestAPI(base_url, "megan")
    robot_api = RobotAPI(rest_api)
    james_api = JamesAPI(rest_api)
    james_api.reset()
    answer = james_api.locate()
    x, y = answer["position"]["x"], answer["position"]["y"]

    warehouse = Warehouse()
    robot = Robot(x, y)
    view = WarehouseView()

    action_manager = ActionManager(robot, robot_api, warehouse)
    action_manager.scan_near()
    action_manager.scan_far()

    presenter = Presenter(robot_api, james_api, warehouse, robot, view)
    view.set_presenter(presenter)
    view.run()


if __name__ == "__main__":
    main()

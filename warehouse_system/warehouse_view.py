from threading import Thread

import pygame

from warehouse_system.cstes import BoundaryTypes, RoomTypes


class WarehouseView:
    def __init__(self):
        self._clock = pygame.time.Clock()
        self.presenter = None
        self.screen_width = 800
        self.screen_height = 400
        self.grid_size_x = 10
        self.grid_size_y = 10
        self.cell_size = 40
        self.robot_x = 1
        self.robot_y = 1

        self.colors = {
            "field": (155, 171, 186),
            "ramp": (243, 200, 103),
            "robot": (242, 116, 97),
            "cell": (230, 230, 230),
            "wall": (79, 81, 83),
            "background": (255, 255, 255),
        }

        pygame.font.init()
        self.game_font_text = pygame.font.SysFont("Serif", 15)
        self.game_font_header = pygame.font.SysFont("Serif", 20, bold=True)
        self.screen = pygame.display.set_mode((self.screen_width, self.screen_height))
        pygame.display.set_caption("Warehouse Manager")

    def set_presenter(self, presenter):
        self.presenter = presenter

    def wait(self, time_in_ms):
        pygame.time.delay(time_in_ms)

    def draw(self):
        self.screen.fill(self.colors["background"])
        self.draw_grid()
        self.draw_rooms()
        self.draw_robot()
        self.draw_walls()
        self.draw_metrics()
        pygame.display.flip()

    def draw_grid(self):
        for x in range(self.grid_size_x):
            for y in range(self.grid_size_y):
                rect = pygame.Rect(
                    x * self.cell_size,
                    y * self.cell_size,
                    self.cell_size,
                    self.cell_size,
                )
                pygame.draw.rect(self.screen, self.colors["cell"], rect, 1)

    def draw_rooms(self):
        for x in range(self.grid_size_x):
            for y in range(self.grid_size_y):
                rect = pygame.Rect(
                    x * self.cell_size,
                    y * self.cell_size,
                    self.cell_size,
                    self.cell_size,
                )
                room_type = self.presenter.warehouse.get_room_type(x, y)
                match room_type:
                    case RoomTypes.FIELD:
                        pygame.draw.rect(self.screen, self.colors["field"], rect)
                    case RoomTypes.RAMP:
                        pygame.draw.rect(self.screen, self.colors["ramp"], rect)

    def draw_walls(self):
        for x in range(self.grid_size_x):
            for y in range(self.grid_size_y):
                boundaries = self.presenter.warehouse.get_room(x, y).get_boundaries()
                for direction, boundary in enumerate(boundaries):
                    if boundary == BoundaryTypes.WALL:
                        self.draw_wall(x, y, direction)

    def draw_wall(self, x, y, direction):
        start_pos = end_pos = None

        if direction == 0:  # Top
            start_pos = (x * self.cell_size, y * self.cell_size)
            end_pos = ((x + 1) * self.cell_size, y * self.cell_size)
        elif direction == 1:  # Right
            start_pos = ((x + 1) * self.cell_size, y * self.cell_size)
            end_pos = ((x + 1) * self.cell_size, (y + 1) * self.cell_size)
        elif direction == 2:  # Bottom
            start_pos = (x * self.cell_size, (y + 1) * self.cell_size)
            end_pos = ((x + 1) * self.cell_size, (y + 1) * self.cell_size)
        elif direction == 3:  # Left
            start_pos = (x * self.cell_size, y * self.cell_size)
            end_pos = (x * self.cell_size, (y + 1) * self.cell_size)

        if start_pos and end_pos:
            pygame.draw.line(self.screen, self.colors["wall"], start_pos, end_pos, 3)

    def draw_robot(self):
        robot_coords = self.presenter.robot.get_coord()
        self.robot_x = robot_coords[0]
        self.robot_y = robot_coords[1]
        pygame.draw.circle(
            self.screen,
            self.colors["robot"],
            (self.robot_x * self.cell_size + self.cell_size // 2, self.robot_y * self.cell_size + self.cell_size // 2),
            self.cell_size // 2.5,
        )

    def draw_metrics(self):
        # Description texts
        robot_header_text = self.game_font_header.render("Robot: ", False, (0, 0, 0))
        robot_location_text = self.game_font_text.render("Location: ", False, (0, 0, 0))
        robot_inventory_text = self.game_font_text.render("Inventory: ", False, (0, 0, 0))

        room_header_text = self.game_font_header.render("Room: ", False, (0, 0, 0))
        room_storage_text = self.game_font_text.render("Inventory: ", False, (0, 0, 0))
        room_type_text = self.game_font_text.render("Type: ", False, (0, 0, 0))
        room_max_capacity_text = self.game_font_text.render("Max Capacity: ", False, (0, 0, 0))
        room_remaining_capacity_text = self.game_font_text.render("Capacity: ", False, (0, 0, 0))

        # Metrics values
        robot = self.presenter.robot
        room = self.presenter.warehouse.get_room(*robot.get_coord())

        robot_location_value = self.game_font_text.render(str(robot.get_coord()), False, (0, 0, 0))
        robot_inventory_value = self.game_font_text.render(robot.get_storage(), False, (0, 0, 0))

        room_storage_value = self.game_font_text.render(str(room.get_storage()), False, (0, 0, 0))
        room_type_value = self.game_font_text.render(str(room.get_room_type()), False, (0, 0, 0))
        room_max_cap_value = self.game_font_text.render(str(room.get_max_capacity()), False, (0, 0, 0))
        room_cap_value = self.game_font_text.render(str(room.get_remaining_capacity()), False, (0, 0, 0))

        # Blit description texts
        self.screen.blit(robot_header_text, (410, 10))
        self.screen.blit(robot_location_text, (410, 35))
        self.screen.blit(robot_inventory_text, (410, 55))

        self.screen.blit(room_header_text, (410, 80))
        self.screen.blit(room_storage_text, (410, 105))
        self.screen.blit(room_type_text, (410, 125))
        self.screen.blit(room_max_capacity_text, (410, 145))
        self.screen.blit(room_remaining_capacity_text, (410, 165))

        # Blit metrics values
        self.screen.blit(robot_location_value, (500, 35))
        self.screen.blit(robot_inventory_value, (500, 55))
        self.screen.blit(room_storage_value, (500, 105))
        self.screen.blit(room_type_value, (500, 125))
        self.screen.blit(room_max_cap_value, (500, 145))
        self.screen.blit(room_cap_value, (500, 165))
        # Metrics Update Screen
        self.screen.blit(robot_location_value, (380, 35))
        self.screen.blit(robot_inventory_value, (380, 55))

    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return

                thread = Thread(target=self.presenter.handle_event, args=(event,))
                thread.start()
                while thread.is_alive():
                    self._clock.tick(60)
                    self.draw()
                thread.join()

            self.draw()
            self._clock.tick(60)


if __name__ == "__main__":
    pygame.init()
    pygame.font.init()
    warehouse_view = WarehouseView(None)

    running = True
    clock = pygame.time.Clock()

    pygame.quit()

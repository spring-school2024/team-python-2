class JamesAPI:
    def __init__(self, rest_api) -> None:
        self.rest_api = rest_api
        self.instance = "james"

    def count(self):
        action = "count"
        content = self.rest_api.get(instance=self.instance, action=action)
        return content

    def fill_warehouse(self):
        action = "fill_warehouse"
        content = self.rest_api.put(instance=self.instance, action=action)
        return content

    def hide(self):
        action = "hide"
        content = self.rest_api.put(instance=self.instance, action=action)
        return content

    def locate(self):
        action = "locate"
        content = self.rest_api.get(instance=self.instance, action=action)
        return content

    def reset(self):
        action = "reset"
        content = self.rest_api.put(instance=self.instance, action=action)
        return content

    def switch_location(self, location: str):
        action = "switch_location" + "/" + location.strip()
        content = self.rest_api.put(instance=self.instance, action=action)
        return content

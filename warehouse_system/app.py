import argparse
import logging

from warehouse_system import configure_logging
from warehouse_system.james_api import JamesAPI
from warehouse_system.parser_configurator import ParserConfigurator
from warehouse_system.rest_api import RestAPI
from warehouse_system.robot_api import RobotAPI

logger = logging.getLogger(__name__)


def execute_command(args, robot_api, james_api):
    command_name_to_function = {
        "inspection": lambda args: robot_api.inspection(),
        "move": lambda args: robot_api.move(args.direction),
        "pick": lambda args: robot_api.pick(),
        "place": lambda args: robot_api.place(),
        "scan_near": lambda args: robot_api.scan_near(),
        "scan_far": lambda args: robot_api.scan_far(),
        "count": lambda args: james_api.count(),
        "fill_warehouse": lambda args: james_api.fill_warehouse(),
        "hide": lambda args: james_api.hide(),
        "locate": lambda args: james_api.locate(),
        "reset": lambda args: james_api.reset(),
        "switch_location": lambda args: james_api.switch_location(args.location),
        # "deliver": lambda args: self.api_requests.deliver_request(),
        # "retrieve": lambda args: self.api_requests.retrieve_request(),
    }
    content = command_name_to_function[args.command](args)
    logger.info(content)


def main():
    configure_logging()
    base_url = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/api/"

    parser = argparse.ArgumentParser()
    configurator = ParserConfigurator(parser)
    configurator.configure_parser()
    args = parser.parse_args()

    rest_api = RestAPI(base_url, args.session)
    robot_api = RobotAPI(rest_api)
    james_api = JamesAPI(rest_api)

    execute_command(args, robot_api, james_api)


if __name__ == "__main__":
    main()

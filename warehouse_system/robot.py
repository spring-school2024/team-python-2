import json
import logging

from warehouse_system.cstes import Directions


class Robot:
    def __init__(self, x=0, y=0):
        self._x = x
        self._y = y
        self._storage = None

    def move(self, direction):
        match direction:
            case Directions.NORTH:
                new_x = self._x
                new_y = self._y - 1
            case Directions.EAST:
                new_x = self._x + 1
                new_y = self._y
            case Directions.SOUTH:
                new_x = self._x
                new_y = self._y + 1
            case Directions.WEST:
                new_x = self._x - 1
                new_y = self._y
        self.set_coord(new_x, new_y)
        return new_x, new_y

    def set_coord(self, x, y):
        self._x = x
        self._y = y

    def get_coord(self):
        return tuple((self._x, self._y))

    def store_item(self, itemName):
        if self._storage is not None:
            logging.warning("Robot can only hold one item at a time.")
            return False
        else:
            self._storage = itemName
            return True

    def deliver_item(self):
        if self._storage is None:
            logging.warning("Robot has no item to deliver.")
            return False
        item = self._storage
        self._storage = None
        return item

    def get_storage(self):
        return self._storage

    @property
    def coord(self):
        return (self._x, self._y)

    def serialize(self):
        return {"x": self._x, "y": self._y, "storage": self._storage}

    @classmethod
    def deserialize(cls, content):
        robot = cls(content["x"], content["y"])
        robot._storage = content["storage"]
        return robot

    def dump(self, fp):
        serialized_data = self.serialize()
        json.dump(serialized_data, fp)

    @classmethod
    def load(cls, fp):
        content = json.load(fp)
        return cls.deserialize(content)

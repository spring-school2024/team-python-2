from warehouse_system.cstes import Directions
from warehouse_system.rest_api import RestAPI


class RobotAPI:
    def __init__(self, rest_api: RestAPI) -> None:
        self.rest_api = rest_api
        self.instance = "bot"

    def inspection(self):
        action = ""
        content = self.rest_api.get(instance=self.instance, action=action)
        return content

    def move(self, direction: str):
        if isinstance(direction, str):
            direction = direction.strip().lower()
            direction = Directions(direction)
        action = "move" + "/" + direction.value
        content = self.rest_api.put(instance=self.instance, action=action)
        return content

    def pick(self):
        action = "pick"
        content = self.rest_api.put(instance=self.instance, action=action)
        return content

    def place(self):
        action = "place"
        content = self.rest_api.put(instance=self.instance, action=action)
        return content

    def scan_near(self):
        action = "scan/near"
        content = self.rest_api.get(instance=self.instance, action=action)
        return content

    def scan_far(self):
        action = "scan/far"
        content = self.rest_api.get(instance=self.instance, action=action)
        return content

import logging

import requests


class RestAPI:
    def __init__(self, base_url: str, session_id: str):
        self.base_url = base_url.lower()
        self.session_id = session_id.lower()

    def get(self, instance: str, action: str):
        url = self.build_url(self.base_url, instance, self.session_id, action)
        response = requests.get(url=url)
        self.check_valid_request(int(response.status_code))

        return response.json()

    def put(self, instance: str, action: str):
        url = self.build_url(self.base_url, instance, self.session_id, action)
        response = requests.put(url=url)
        self.check_valid_request(int(response.status_code))

        return response.json()

    def post(self, instance: str, action: str, data: dict):
        url = self.build_url(self.base_url, instance, self.session_id, action)
        response = requests.post(url=url, json=data)
        self.check_valid_request(int(response.status_code))

        return response.json()

    def set_base_url(self, base_url: str):
        self.base_url = base_url

    def set_session_id(self, session_id: str):
        self.session_id = session_id

    def get_base_url(self):
        return self.base_url

    def get_session_id(self):
        return self.session_id

    def build_url(self, base_url: str, instance: str, session_id: str, action: str):
        url = f"{base_url}/{instance}/{session_id}/{action}"
        return url

    def check_valid_request(self, status_code: str):
        if not 200 <= status_code < 300:
            logging.warning(f"Request returned an invalid status_code: {status_code}")
        return True

import logging

from warehouse_system.cstes import BoundaryTypes, Directions, RoomTypes, direction_to_index

logger = logging.getLogger(__name__)


def get_other_room_coord(x, y, direction, offset):
    match direction:
        case Directions.NORTH:
            return x, y - offset
        case Directions.EAST:
            return x + offset, y
        case Directions.SOUTH:
            return x, y + offset
        case Directions.WEST:
            return x - offset, y


class ActionManager:
    def __init__(self, robot, robot_api, warehouse):
        self.robot = robot
        self.robot_api = robot_api
        self.warehouse = warehouse

    def _check_answer(self, answer, err_message):
        valid = answer["request_status"] == "ok"
        if not valid:
            message = answer["message"]
            logger.warning(f"{err_message} {message}")
        return valid

    def move(self, direction):
        # check movement is possible
        x, y = self.robot.get_coord()
        boundaries = self.warehouse.get_boundaries(x, y)
        boundary = boundaries[direction_to_index[direction]]
        if boundary is BoundaryTypes.WALL:
            logger.warning("Can not move, there is a wall")
            return False
        # send REST request
        answer = self.robot_api.move(direction)
        if not self._check_answer(answer, "Could not move:"):
            return False
        # update robot state
        self.robot.move(direction)
        return True

    def place(self):
        x, y = self.robot.get_coord()
        room_type = self.warehouse.get_room_type(x, y)
        robot_item = self.robot.get_storage()
        if robot_item is None:
            logger.warning("Can not place: nothing in the robot")
            return False
        match room_type:
            case RoomTypes.UNKNOWN:
                self.scan_near()
                self.place()
                return False

            case RoomTypes.RAMP:
                pass

            case RoomTypes.FIELD:
                warehouse_item = self.warehouse.get_stored_item_type(x, y)
                if warehouse_item is not None and warehouse_item != robot_item:
                    logger.warning("Can not place the object, incorrect item type ({warehouse_item=} {robot_item=})")
                    return False
                remaining_capacity = self.warehouse.get_remaining_capacity(x, y)
                if remaining_capacity <= 0:
                    logger.warning(f"Not enough space in the storage ({remaining_capacity=})")
                    return False

        answer = self.robot_api.place()
        if not self._check_answer(answer, "Could not place:"):
            return False

        robot_item = self.robot.deliver_item()
        self.warehouse.add_storage_item(x, y, robot_item)
        return True

    def pick(self):
        x, y = self.robot.get_coord()
        robot_item = self.robot.get_storage()
        if robot_item is not None:
            logger.warning("Can not pick: robot has busy hands")
            return False

        warehouse_item = self.warehouse.get_stored_item_type(x, y)
        if warehouse_item is None:
            logger.warning("Could not pick: nothing to pick")
            return False

        answer = self.robot_api.pick()
        if not self._check_answer(answer, "Could not pick:"):
            return False

        warehouse_item = self.warehouse.pop_storage_item(x, y)
        self.robot.store_item(warehouse_item)
        return True

    def scan_near(self):
        x, y = self.robot.get_coord()
        answer = self.robot_api.scan_near()
        if not self._check_answer(answer, "Could not scan (near):"):
            return False
        self.warehouse.set_room_type(x, y, RoomTypes(answer["field_info"]["id"].lower()))
        self.warehouse.set_storage(x, y, answer["field_info"]["shelf_inventory"])
        self.warehouse.set_max_capacity(x, y, answer["field_info"]["max_capacity"])
        for direction in Directions:
            boundary_type = BoundaryTypes.WALL if answer["field_info"]["walls"][direction.value] else BoundaryTypes.PATH
            self.warehouse.set_boundary(x, y, direction, boundary_type)
        return True

    def scan_far(self):
        x, y = self.robot.get_coord()
        answer = self.robot_api.scan_far()
        if not self._check_answer(answer, "Could not scan (far):"):
            return False

        for direction in Directions:
            for room_offset in range(answer["scan_info"][direction.value]):
                other_x, other_y = get_other_room_coord(x, y, direction, room_offset)
                self.warehouse.set_boundary(other_x, other_y, direction, BoundaryTypes.PATH)
            other_x, other_y = get_other_room_coord(x, y, direction, answer["scan_info"][direction.value])
            self.warehouse.set_boundary(other_x, other_y, direction, BoundaryTypes.WALL)
        return True

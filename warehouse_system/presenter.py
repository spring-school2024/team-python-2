import logging

import pygame

from warehouse_system.action_manager import ActionManager
from warehouse_system.admin import Admin
from warehouse_system.cstes import Directions
from warehouse_system.planner import Planner
from warehouse_system.sequence_manager import SequenceManager

logger = logging.getLogger(__name__)


class Presenter:
    def __init__(self, robot_api, james_api, warehouse, robot, view):
        self.robot_api = robot_api
        self.james_api = james_api
        self.warehouse = warehouse
        self.robot = robot
        self.view = view
        self.admin = Admin()
        self.planner = Planner()
        self.action_manager = ActionManager(robot, robot_api, warehouse)
        self.sequence_manager = SequenceManager(robot, robot_api, warehouse, self.planner, self.action_manager)

    def handle_event(self, event):
        match event:
            case object(type=pygame.KEYDOWN, key=pygame.K_r):
                self.sequence_manager.go_to_ramp()
            case object(type=pygame.KEYDOWN, key=pygame.K_UP):
                self.action_manager.move(Directions.NORTH)
                self.action_manager.scan_near()
                self.action_manager.scan_far()
            case object(type=pygame.KEYDOWN, key=pygame.K_RIGHT):
                self.action_manager.move(Directions.EAST)
                self.action_manager.scan_near()
                self.action_manager.scan_far()
            case object(type=pygame.KEYDOWN, key=pygame.K_DOWN):
                self.action_manager.move(Directions.SOUTH)
                self.action_manager.scan_near()
                self.action_manager.scan_far()
            case object(type=pygame.KEYDOWN, key=pygame.K_LEFT):
                self.action_manager.move(Directions.WEST)
                self.action_manager.scan_near()
                self.action_manager.scan_far()

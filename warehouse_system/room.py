import json
from typing import List

from warehouse_system.cstes import BoundaryTypes, Directions, RoomTypes


class Room:
    def __init__(self):
        # north, east, south, west
        self.structure = [
            BoundaryTypes.UNKNOWN,
            BoundaryTypes.UNKNOWN,
            BoundaryTypes.UNKNOWN,
            BoundaryTypes.UNKNOWN,
        ]
        self.room_type = RoomTypes.UNKNOWN
        self.storage = []
        self.max_capacity = 0

    def set_room_type(self, room_type):
        self.room_type = room_type

    def get_room_type(self):
        return self.room_type

    def get_stored_item_type(self):
        if self.get_storage_count() >= 1:
            return self.storage[0]
        return None

    def pop_storage_item(self):
        return self.storage.pop()

    def set_storage(self, items: List[str]):
        self.storage = items

    def get_storage(self):
        return self.storage

    def get_storage_count(self) -> int:
        return len(self.storage)

    def add_storage_item(self, item: str):
        self.storage.append(item)

    def add_storage_items(self, items: List[str]):
        self.storage.extend(items)

    def set_max_capacity(self, max_capacity):
        match self.room_type:
            case RoomTypes.FIELD:
                self.max_capacity = max_capacity
            case RoomTypes.RAMP:
                self.max_capacity = 0
            case RoomTypes.UNKNOWN:
                self.max_capacity = 0

    def get_max_capacity(self):
        return self.max_capacity

    def get_remaining_capacity(self):
        remaining_capacity = 0
        match self.room_type:
            case RoomTypes.FIELD:
                remaining_capacity = self.max_capacity - self.get_storage_count()
            case RoomTypes.RAMP:
                remaining_capacity = 0
            case RoomTypes.UNKNOWN:
                remaining_capacity = 0

        return remaining_capacity

    def set_boundary(self, direction, boundaries_type):
        match direction:
            case Directions.NORTH:
                self.structure[0] = boundaries_type
            case Directions.EAST:
                self.structure[1] = boundaries_type
            case Directions.SOUTH:
                self.structure[2] = boundaries_type
            case Directions.WEST:
                self.structure[3] = boundaries_type
        return True

    def set_boundaries(self, N_boundaries_type, E_boundaries_type, S_boundaries_type, W_boundaries_type):
        self.structure[0] = N_boundaries_type
        self.structure[1] = E_boundaries_type
        self.structure[2] = S_boundaries_type
        self.structure[3] = W_boundaries_type

    def delete_boundary(self, direction):
        match direction:
            case Directions.NORTH:
                self.structure[0] = BoundaryTypes.UNKNOWN
            case Directions.EAST:
                self.structure[1] = BoundaryTypes.UNKNOWN
            case Directions.SOUTH:
                self.structure[2] = BoundaryTypes.UNKNOWN
            case Directions.WEST:
                self.structure[3] = BoundaryTypes.UNKNOWN
        return True

    def get_boundaries(self):
        return tuple(self.structure)

    def get_n_boundaries(self, boundary_type):
        return self.structure.count(boundary_type)

    def serialize(self):
        return {
            "structure": [boundary.value for boundary in self.structure],
            "room_type": self.room_type.value,
            "storage": self.storage,
            "max_capacity": self.max_capacity,
        }

    @classmethod
    def deserialize(cls, content):
        room = cls()
        room.structure = [BoundaryTypes(boundary) for boundary in content["structure"]]
        room.room_type = RoomTypes(content["room_type"])
        room.storage = content["storage"]
        room.max_capacity = content.get("max_capacity", None)
        return room

    def dump(self, fp):
        serialized_data = self.serialize()
        json.dump(serialized_data, fp)

    def load(self, fp):
        content = json.load(fp)
        self.structure = [BoundaryTypes(boundary) for boundary in content["structure"]]
        self.room_type = RoomTypes(content["room_type"])
        self.storage = content["storage"]
        self.max_capacity = content.get("max_capacity", None)

import enum


class Directions(enum.Enum):
    NORTH = "north"
    EAST = "east"
    SOUTH = "south"
    WEST = "west"


class BoundaryTypes(enum.Enum):
    WALL = "wall"
    PATH = "path"
    UNKNOWN = "unknown"


class RoomTypes(enum.Enum):
    FIELD = "field"
    RAMP = "ramp"
    UNKNOWN = "unknown"


direction_to_index = {
    Directions.NORTH: 0,
    Directions.EAST: 1,
    Directions.SOUTH: 2,
    Directions.WEST: 3,
}

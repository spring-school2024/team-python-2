import logging


def configure_logging():
    logging.basicConfig(
        level=logging.DEBUG,
        format="{relativeCreated:13.2f} {levelname:>8} {name:>35.35}:{lineno:4d} │  {message}",
        style="{",
    )

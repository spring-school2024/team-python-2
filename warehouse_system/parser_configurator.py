import argparse
import logging

logger = logging.getLogger(__name__)

DEFAULT_SESSION_NAME = "megan"

COMMANDS = [
    ("inspection", "Inspect basic information about the bot"),
    ("move", "Move the robot north, east, west or south"),
    ("pick", "Robot picks up object"),
    ("place", "Robot places object"),
    ("scan_near", "Scans the near surroundings of the robot"),
    ("scan_far", "Scans the far surroundings of the robot"),
    ("count", "Counts the items in warehouse and ramp"),
    ("fill_warehouse", "Fills warehouse with items"),
    ("hide", "Hides robot at random location"),
    ("locate", "Locates the coordinates of the robot"),
    ("reset", "Resets the application on the server"),
    ("switch_location", "Switches between different maps"),
    ("deliver", "Delivers new goods to the ramp"),
    ("retrieve", "Retrieves goods from the ramp"),
]

DIRECTIONS = ["north", "east", "south", "west"]
LOCATIONS = ["Default", "Circle", "BigMap"]


class ParserConfigurator:
    def __init__(self, parser: argparse.ArgumentParser):
        self.parser = parser
        self.parsers = {}

    def configure_command_parser(self):
        subparsers = self.parser.add_subparsers(dest="command")
        self.parsers["command"] = {
            name: subparsers.add_parser(name, help=desc, description=desc) for name, desc in COMMANDS
        }
        return self.parsers["command"]

    def configure_directions_parsers(self):
        subparsers = self.parsers["command"]["move"].add_subparsers(dest="direction")
        self.parsers["direction"] = {
            direction: subparsers.add_parser(
                direction, help=f"Move the robot towards {direction}", description=f"Move the robot towards {direction}"
            )
            for direction in DIRECTIONS
        }
        return self.parsers["direction"]

    def configure_location_parsers(self):
        subparsers = self.parsers["command"]["switch_location"].add_subparsers(dest="location")
        self.parsers["location"] = {
            location: subparsers.add_parser(
                location,
                help=f"The following map gets loaded: {location}",
                description=f"The following map gets loaded: {location}",
            )
            for location in LOCATIONS
        }
        return self.parsers["location"]

    def configure_parser(self):
        self.parser.add_argument("--session", type=str, help="Session name", default=DEFAULT_SESSION_NAME)
        self.configure_command_parser()
        self.configure_directions_parsers()
        self.configure_location_parsers()

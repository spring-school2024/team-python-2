import pygame
from james_api import JamesAPI
from rest_api import RestAPI
from robot_api import RobotAPI

# Constants
GRID_SIZE = 20
CELL_SIZE = 30  # Size of each cell in pixels
PLAYER_OFFSET_X = 5
PLAYER_OFFSET_Y = 5
SCREEN_SIZE = GRID_SIZE * CELL_SIZE
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GREY = (230, 230, 230)


class GameGui:
    def __init__(self, robot_api, james_api):
        self.robot_api = robot_api
        self.james_api = james_api
        self.grid = [[None for _ in range(GRID_SIZE)] for _ in range(GRID_SIZE)]
        self.walls = set()
        self.robot_position = None
        self.ramp_position = None
        self.screen = pygame.display.set_mode((SCREEN_SIZE, SCREEN_SIZE))
        pygame.display.set_caption("Warehouse Manager")

        self.set_robot_position(x=self.james_api.locate()["position"]["x"], y=self.james_api.locate()["position"]["y"])
        self.update_surroundings()

    def add_storage_cell(self, x, y):
        if self.is_within_bounds(x, y):
            self.grid[y][x] = "storage"

    def add_ramp_cell(self, x, y):
        if self.is_within_bounds(x, y) and not self.ramp_position:
            self.grid[y][x] = "ramp"
            self.ramp_position = (x, y)

    def add_wall(self, x1, y1, x2, y2):
        if self.is_within_bounds(x1, y1) and self.is_within_bounds(x2, y2):
            self.walls.add(((x1, y1), (x2, y2)))
            self.walls.add(((x2, y2), (x1, y1)))

    def set_robot_position(self, x, y):
        x = x + PLAYER_OFFSET_X
        y = y + PLAYER_OFFSET_Y
        if self.is_within_bounds(x, y):
            self.robot_position = (x, y)

    def update_field_type(self, x, y, field_type):
        if self.is_within_bounds(x, y):
            if field_type == "field":
                self.grid[y][x] = "storage"
            elif field_type == "Ramp":
                self.grid[y][x] = "ramp"

    def update_walls(self, x, y, walls):
        if walls["north"]:
            self.add_wall(x - 0.5, y - 0.5, x + 0.5, y - 0.5)
        if walls["east"]:
            self.add_wall(x + 0.5, y + 0.5, x + 0.5, y - 0.5)
        if walls["south"]:
            self.add_wall(x - 0.5, y + 0.5, x + 0.5, y + 0.5)
        if walls["west"]:
            self.add_wall(x - 0.5, y + 0.5, x - 0.5, y - 0.5)

    def update_surroundings(self):
        near_scan_data = self.robot_api.scan_near()["field_info"]
        field_type = near_scan_data["id"]
        near_walls = near_scan_data["walls"]
        self.update_field_type(*self.robot_position, field_type)
        self.update_walls(*self.robot_position, near_walls)

    def move_robot(self, direction):
        if self.robot_position:
            x, y = self.robot_position
            if direction == "north":
                new_pos = (x, y - 1)
            elif direction == "south":
                new_pos = (x, y + 1)
            elif direction == "west":
                new_pos = (x - 1, y)
            elif direction == "east":
                new_pos = (x + 1, y)
            else:
                return

            if self.can_move_to(new_pos, direction):
                self.robot_position = new_pos
                self.update_surroundings()

    def can_move_to(self, position, direction):
        x, y = position
        if not self.is_within_bounds(x, y):
            return False
        if self.is_wall_between(self.robot_position, position):
            return False
        if self.robot_api.move(direction)["request_status"] == "failed":
            return False
        return True

    def is_within_bounds(self, x, y):
        return 0 <= x < GRID_SIZE and 0 <= y < GRID_SIZE

    def is_wall_between(self, pos1, pos2):
        return (pos1, pos2) in self.walls

    def draw_grid(self):
        self.screen.fill(WHITE)
        for y in range(GRID_SIZE):
            for x in range(GRID_SIZE):
                rect = pygame.Rect(x * CELL_SIZE, y * CELL_SIZE, CELL_SIZE, CELL_SIZE)
                if self.grid[y][x] == "storage":
                    pygame.draw.rect(self.screen, GREEN, rect)
                elif self.grid[y][x] == "ramp":
                    pygame.draw.rect(self.screen, BLUE, rect)
                pygame.draw.rect(self.screen, GREY, rect, 1)

        for wall in self.walls:
            (x1, y1), (x2, y2) = wall
            x1, y1 = x1 * CELL_SIZE + CELL_SIZE // 2, y1 * CELL_SIZE + CELL_SIZE // 2
            x2, y2 = x2 * CELL_SIZE + CELL_SIZE // 2, y2 * CELL_SIZE + CELL_SIZE // 2
            pygame.draw.line(self.screen, BLACK, (x1, y1), (x2, y2), 5)

        if self.robot_position:
            x, y = self.robot_position
            size_multiplier = 0.9
            # offset_multiplier = 1/size_multiplier
            rect = pygame.Rect(x * CELL_SIZE, y * CELL_SIZE, CELL_SIZE * size_multiplier, CELL_SIZE * size_multiplier)
            pygame.draw.rect(self.screen, RED, rect)

    def run(self):
        running = True
        clock = pygame.time.Clock()

        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_UP:
                        self.move_robot("north")
                    elif event.key == pygame.K_DOWN:
                        self.move_robot("south")
                    elif event.key == pygame.K_LEFT:
                        self.move_robot("west")
                    elif event.key == pygame.K_RIGHT:
                        self.move_robot("east")

                self.draw_grid()
                pygame.display.flip()
                clock.tick(60)

        pygame.quit()


if __name__ == "__main__":
    base_url = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/api/"
    session_id = "megan"

    rest_api = RestAPI(base_url, session_id)
    robot_api = RobotAPI(rest_api)
    james_api = JamesAPI(rest_api)

    james_api.switch_location("Default")
    james_api.reset()

    pygame.init()
    warehouse = GameGui(robot_api, james_api)
    warehouse.run()

import pytest

from warehouse_system.cstes import BoundaryTypes, Directions, RoomTypes
from warehouse_system.room import Room


@pytest.fixture
def room():
    return Room()


def test_room_init(room):
    assert room.structure == [
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
    ]
    assert room.room_type == RoomTypes.UNKNOWN
    assert room.storage == []
    assert room.max_capacity == 0


def test_set_room_type(room):
    room.set_room_type(RoomTypes.FIELD)
    assert room.get_room_type() == RoomTypes.FIELD

    room.set_room_type(RoomTypes.RAMP)
    assert room.get_room_type() == RoomTypes.RAMP

    room.set_room_type(RoomTypes.UNKNOWN)
    assert room.get_room_type() == RoomTypes.UNKNOWN


def test_set_storage(room):
    items = ["item1", "item2"]
    room.set_storage(items)
    assert room.get_storage() == items


def test_get_storage_count(room):
    items = ["item1", "item2", "item3"]
    room.set_storage(items)
    assert room.get_storage_count() == 3


def test_add_storage_item(room):
    item = ["item1"]
    room.set_storage(item)
    room.add_storage_item("item2")
    assert room.get_storage() == ["item1", "item2"]


def test_add_storage_items(room):
    items = ["item1", "item2"]
    room.add_storage_items(items)
    assert room.get_storage() == items


def test_set_max_capacity(room):
    room.set_room_type(RoomTypes.FIELD)
    room.set_max_capacity(12)
    assert room.max_capacity == 12

    room.set_room_type(RoomTypes.RAMP)
    room.set_max_capacity(12)
    assert room.max_capacity == 0

    room.set_room_type(RoomTypes.UNKNOWN)
    room.set_max_capacity(12)
    assert room.max_capacity == 0


def test_get_max_capacity(room):
    room.set_room_type(RoomTypes.FIELD)
    room.set_max_capacity(12)
    assert room.get_max_capacity() == 12


def test_get_remaining_capacity_field(room):
    room.set_room_type(RoomTypes.FIELD)
    room.set_max_capacity(12)
    room.set_storage(["item1", "item2", "item3"])
    assert room.get_remaining_capacity() == 9


def test_get_remaining_capacity_ramp(room):
    room.set_room_type(RoomTypes.RAMP)
    room.set_max_capacity(12)
    room.set_storage(["item1", "item2", "item3"])
    assert room.get_remaining_capacity() == 0


def test_get_remaining_capacity_unknown(room):
    room.set_room_type(RoomTypes.UNKNOWN)
    room.set_max_capacity(12)
    room.set_storage(["item1", "item2", "item3"])
    assert room.get_remaining_capacity() == 0


def test_set_boundary(room):
    room.set_boundary(Directions.NORTH, BoundaryTypes.WALL)
    assert room.get_boundaries()[0] == BoundaryTypes.WALL


def test_set_boundaries(room):
    room.set_boundaries(BoundaryTypes.WALL, BoundaryTypes.PATH, BoundaryTypes.UNKNOWN, BoundaryTypes.WALL)
    assert room.get_boundaries() == (BoundaryTypes.WALL, BoundaryTypes.PATH, BoundaryTypes.UNKNOWN, BoundaryTypes.WALL)


def test_delete_boundary(room):
    room.set_boundary(Directions.NORTH, BoundaryTypes.WALL)
    room.delete_boundary(Directions.NORTH)
    assert room.get_boundaries()[0] == BoundaryTypes.UNKNOWN


def test_serialize(room):
    room.set_room_type(RoomTypes.FIELD)
    room.set_storage(["item1"])
    room.set_max_capacity(12)
    room.set_boundaries(BoundaryTypes.WALL, BoundaryTypes.PATH, BoundaryTypes.UNKNOWN, BoundaryTypes.WALL)
    serialized_data = room.serialize()
    expected_data = {
        "structure": ["wall", "path", "unknown", "wall"],
        "room_type": "field",
        "storage": ["item1"],
        "max_capacity": 12,
    }
    assert serialized_data == expected_data


def test_deserialize():
    content = {
        "structure": ["wall", "path", "unknown", "wall"],
        "room_type": "field",
        "storage": ["item1"],
        "max_capacity": 12,
    }
    room = Room.deserialize(content)
    assert room.get_room_type() == RoomTypes.FIELD
    assert room.get_storage() == ["item1"]
    assert room.max_capacity == 12
    assert room.get_boundaries() == (BoundaryTypes.WALL, BoundaryTypes.PATH, BoundaryTypes.UNKNOWN, BoundaryTypes.WALL)


def test_dump_load(tmp_path):
    room = Room()
    room.set_room_type(RoomTypes.FIELD)
    room.set_storage(["item1"])
    room.set_max_capacity(12)
    room.set_boundaries(BoundaryTypes.WALL, BoundaryTypes.PATH, BoundaryTypes.UNKNOWN, BoundaryTypes.WALL)

    file_path = tmp_path / "room.json"
    with open(file_path, "w") as f:
        room.dump(f)

    new_room = Room()
    with open(file_path, "r") as f:
        new_room.load(f)

    assert new_room.get_room_type() == RoomTypes.FIELD
    assert new_room.get_storage() == ["item1"]
    assert new_room.max_capacity == 12
    assert new_room.get_boundaries() == (
        BoundaryTypes.WALL,
        BoundaryTypes.PATH,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.WALL,
    )

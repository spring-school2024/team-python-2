import argparse
from unittest.mock import MagicMock

import pytest

from warehouse_system.app import execute_command
from warehouse_system.james_api import JamesAPI
from warehouse_system.parser_configurator import ParserConfigurator
from warehouse_system.rest_api import RestAPI
from warehouse_system.robot_api import RobotAPI

# Sample command arguments for testing
sample_args = ["--session", "test_session", "move", "north"]


@pytest.fixture
def mock_rest_api():
    return MagicMock(spec=RestAPI)


@pytest.fixture
def mock_robot_api(mock_rest_api):
    return MagicMock(spec=RobotAPI)


@pytest.fixture
def mock_james_api(mock_rest_api):
    return MagicMock(spec=JamesAPI)


@pytest.fixture
def parser():
    return argparse.ArgumentParser()


@pytest.fixture
def configurator(parser):
    configurator = ParserConfigurator(parser)
    configurator.configure_parser()
    return configurator


def test_configure_parser(configurator):
    args = configurator.parser.parse_args(sample_args)
    assert args.session == "test_session"
    assert args.command == "move"
    assert args.direction == "north"


def test_execute_command_inspection(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["inspection"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_robot_api.inspection.assert_called_once()


def test_execute_command_move(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["move", "north"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_robot_api.move.assert_called_once_with("north")


def test_execute_command_pick(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["pick"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_robot_api.pick.assert_called_once()


def test_execute_command_place(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["place"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_robot_api.place.assert_called_once()


def test_execute_command_scan_near(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["scan_near"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_robot_api.scan_near.assert_called_once()


def test_execute_command_scan_far(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["scan_far"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_robot_api.scan_far.assert_called_once()


def test_execute_command_count(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["count"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_james_api.count.assert_called_once()


def test_execute_command_fill_warehouse(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["fill_warehouse"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_james_api.fill_warehouse.assert_called_once()


def test_execute_command_hide(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["hide"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_james_api.hide.assert_called_once()


def test_execute_command_locate(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["locate"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_james_api.locate.assert_called_once()


def test_execute_command_reset(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["reset"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_james_api.reset.assert_called_once()


def test_execute_command_switch_location(configurator, mock_robot_api, mock_james_api):
    args = configurator.parser.parse_args(["switch_location", "Default"])
    execute_command(args, mock_robot_api, mock_james_api)
    mock_james_api.switch_location.assert_called_once_with("Default")

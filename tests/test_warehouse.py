import pytest

from warehouse_system.cstes import BoundaryTypes, Directions
from warehouse_system.warehouse import Warehouse

wrong_coord_data_type = [None, "str"]
wrong_direction_data_type = [None, 7]
boundaries = (BoundaryTypes.PATH, BoundaryTypes.WALL, BoundaryTypes.UNKNOWN)


@pytest.fixture
def warehouse():
    return Warehouse()


@pytest.mark.parametrize("direction", wrong_direction_data_type)
@pytest.mark.parametrize("coord", wrong_coord_data_type)
@pytest.mark.parametrize("boundary_type", boundaries)
def test_warehouse_add(warehouse, boundary_type, coord, direction):
    assert warehouse.get_boundaries(0, 0) == (
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
    )

    warehouse.set_boundary(0, 0, Directions.SOUTH, boundary_type)
    assert warehouse.get_boundaries(0, 0) == (
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        boundary_type,
        BoundaryTypes.UNKNOWN,
    )
    assert warehouse.get_boundaries(0, 1) == (
        boundary_type,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
    )

    warehouse.set_boundary(0, 0, Directions.NORTH, boundary_type)
    assert warehouse.get_boundaries(0, 0) == (
        boundary_type,
        BoundaryTypes.UNKNOWN,
        boundary_type,
        BoundaryTypes.UNKNOWN,
    )

    with pytest.raises(ValueError):
        warehouse.set_boundary(coord, 0, Directions.NORTH, boundary_type)

    with pytest.raises(ValueError):
        warehouse.set_boundary(0, coord, Directions.NORTH, boundary_type)

    with pytest.raises(ValueError):
        warehouse.set_boundary(0, 0, direction, boundary_type)


@pytest.mark.parametrize("direction", wrong_direction_data_type)
@pytest.mark.parametrize("coord", wrong_coord_data_type)
@pytest.mark.parametrize("boundary_type", boundaries)
def test_warehouse_delete(warehouse, boundary_type, coord, direction):
    assert warehouse.get_boundaries(0, 0) == (
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
    )

    warehouse.set_boundary(0, 0, Directions.SOUTH, BoundaryTypes.WALL)
    warehouse.delete_boundary(0, 0, Directions.SOUTH)
    assert warehouse.get_boundaries(0, 0) == (
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
    )
    assert warehouse.get_boundaries(0, 1) == (
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
    )

    with pytest.raises(ValueError):
        warehouse.delete_boundary(coord, 0, Directions.NORTH)

    with pytest.raises(ValueError):
        warehouse.delete_boundary(0, coord, Directions.NORTH)

    with pytest.raises(ValueError):
        warehouse.delete_boundary(0, 0, direction)


@pytest.mark.parametrize("direction", wrong_direction_data_type)
@pytest.mark.parametrize("coord", wrong_coord_data_type)
@pytest.mark.parametrize("boundary_type", boundaries)
def test_warehouse_set(warehouse, boundary_type, coord, direction):
    assert warehouse.get_boundaries(0, 0) == (
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
    )

    warehouse.set_boundaries(0, 0, boundary_type, boundary_type, boundary_type, boundary_type)
    assert warehouse.get_boundaries(0, 0) == (
        boundary_type,
        boundary_type,
        boundary_type,
        boundary_type,
    )
    assert warehouse.get_boundaries(1, 0) == (
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        boundary_type,
    )
    assert warehouse.get_boundaries(0, 1) == (
        boundary_type,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
    )
    assert warehouse.get_boundaries(-1, 0) == (
        BoundaryTypes.UNKNOWN,
        boundary_type,
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
    )
    assert warehouse.get_boundaries(0, -1) == (
        BoundaryTypes.UNKNOWN,
        BoundaryTypes.UNKNOWN,
        boundary_type,
        BoundaryTypes.UNKNOWN,
    )


@pytest.mark.parametrize("direction", wrong_direction_data_type)
@pytest.mark.parametrize("coord", wrong_coord_data_type)
@pytest.mark.parametrize("boundary_type", boundaries)
def test_warehouse_get_n(warehouse, boundary_type, coord, direction):
    warehouse.set_boundaries(0, 0, boundary_type, boundary_type, boundary_type, boundary_type)
    assert warehouse.get_n_boundaries(0, 0, boundary_type) == 4

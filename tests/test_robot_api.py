import pytest  # noqa: F401

from warehouse_system.rest_api import RestAPI
from warehouse_system.robot_api import RobotAPI

TEST_SESSION_ID = "test_session"
INSTANCE = "bot"
TEST_URL = "http://example.com/rest_api"


@pytest.fixture
def robot_api():
    rest_api = RestAPI(TEST_URL, TEST_SESSION_ID)
    return RobotAPI(rest_api)


def construct_url(action):
    return f"{TEST_URL}/{INSTANCE}/{TEST_SESSION_ID}/{action}"


def perform_test(requests_mock, action, get_result, REST):
    json = {"request_status": "ok"}
    url = construct_url(action)
    requests_mock.register_uri(REST, url, json=json)
    result = get_result()
    for key, value in json.items():
        assert result[key] == value


def test_inspection(requests_mock, robot_api):
    perform_test(
        requests_mock,
        "",
        lambda: robot_api.inspection(),
        "GET",
    )


def test_move(requests_mock, robot_api):
    perform_test(
        requests_mock,
        "move/north",
        lambda: robot_api.move("north"),
        "PUT",
    )


def test_pick(requests_mock, robot_api):
    perform_test(
        requests_mock,
        "pick",
        lambda: robot_api.pick(),
        "PUT",
    )


def test_place(requests_mock, robot_api):
    perform_test(
        requests_mock,
        "place",
        lambda: robot_api.place(),
        "PUT",
    )


def test_near(requests_mock, robot_api):
    perform_test(
        requests_mock,
        "scan/near",
        lambda: robot_api.scan_near(),
        "GET",
    )


def test_far(requests_mock, robot_api):
    perform_test(
        requests_mock,
        "scan/far",
        lambda: robot_api.scan_far(),
        "GET",
    )

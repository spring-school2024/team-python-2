import json
from io import StringIO

import pytest

from warehouse_system.robot import Robot


@pytest.fixture
def robot():
    return Robot()


def test_initial_coordinates(robot):
    assert robot.get_coord() == (0, 0)
    assert robot.coord == (0, 0)


def test_set_and_get_coordinates(robot):
    robot.set_coord(5, 10)
    assert robot.get_coord() == (5, 10)
    assert robot.coord == (5, 10)


def test_store_item(robot, caplog):
    robot.store_item("item1")
    assert robot.get_storage() == "item1"


def test_deliver_item(robot, caplog):
    robot.store_item("item1")
    item = robot.deliver_item()
    assert item == "item1"
    assert robot.get_storage() is None


def test_serialize(robot):
    robot.set_coord(5, 10)
    robot.store_item("item1")
    serialized = robot.serialize()
    expected = {"x": 5, "y": 10, "storage": "item1"}
    assert serialized == expected


def test_deserialize():
    content = {"x": 5, "y": 10, "storage": "item1"}
    robot = Robot.deserialize(content)
    assert robot.get_coord() == (5, 10)
    assert robot.get_storage() == "item1"


def test_dump(robot):
    robot.set_coord(5, 10)
    robot.store_item("item1")
    fp = StringIO()
    robot.dump(fp)
    fp.seek(0)
    serialized_data = json.load(fp)
    expected = {"x": 5, "y": 10, "storage": "item1"}
    assert serialized_data == expected


def test_load():
    content = {"x": 5, "y": 10, "storage": "item1"}
    fp = StringIO(json.dumps(content))
    robot = Robot.load(fp)
    assert robot.get_coord() == (5, 10)
    assert robot.get_storage() == "item1"

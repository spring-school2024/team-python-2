import pytest  # noqa: F401

from warehouse_system.james_api import JamesAPI
from warehouse_system.rest_api import RestAPI

TEST_SESSION_ID = "test_session"
INSTANCE = "james"
TEST_URL = "http://example.com/rest_api"


@pytest.fixture
def james_api():
    rest_api = RestAPI(TEST_URL, TEST_SESSION_ID)
    return JamesAPI(rest_api)


def construct_url(action):
    return f"{TEST_URL}/{INSTANCE}/{TEST_SESSION_ID}/{action}"


def perform_test(requests_mock, action, get_result, REST):
    json = {"request_status": "ok"}
    url = construct_url(action)
    requests_mock.register_uri(REST, url, json=json)
    result = get_result()
    for key, value in json.items():
        assert result[key] == value


def test_count(requests_mock, james_api):
    perform_test(
        requests_mock,
        "count",
        lambda: james_api.count(),
        "GET",
    )


def test_fill_warehouse(requests_mock, james_api):
    perform_test(
        requests_mock,
        "fill_warehouse",
        lambda: james_api.fill_warehouse(),
        "PUT",
    )


def test_hide(requests_mock, james_api):
    perform_test(
        requests_mock,
        "hide",
        lambda: james_api.hide(),
        "PUT",
    )


def test_locate(requests_mock, james_api):
    perform_test(
        requests_mock,
        "locate",
        lambda: james_api.locate(),
        "GET",
    )


def test_reset(requests_mock, james_api):
    perform_test(
        requests_mock,
        "reset",
        lambda: james_api.reset(),
        "PUT",
    )


def test_switch_location(requests_mock, james_api):
    perform_test(
        requests_mock,
        "switch_location/Circle",
        lambda: james_api.switch_location("Circle"),
        "PUT",
    )
